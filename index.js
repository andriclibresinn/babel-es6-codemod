const { JSONPath } = require('jsonpath-plus');

const sample = {
  "store": {
    "book": [
      {
        "category": "reference",
        "author": "Nigel Rees",
        "title": "Sayings of the Century",
        "price": 8.95
      },
      {
        "category": "fiction",
        "author": "Evelyn Waugh",
        "title": "Sword of Honour",
        "price": 12.99
      },
      {
        "category": "fiction",
        "author": "Herman Melville",
        "title": "Moby Dick",
        "isbn": "0-553-21311-3",
        "price": 8.99
      },
      {
        "category": "fiction",
        "author": "J. R. R. Tolkien",
        "title": "The Lord of the Rings",
        "isbn": "0-395-19395-8",
        "price": 22.99
      }
    ],
      "bicycle": {
        "color": "red",
        "price": 19.95
    }
  }
}

const result = JSONPath({
  path: '$.store.book[0].author',   // Would prefer: '//store/book[0]/author'
  path: '$.store.book[*].price',    // Would prefer: '//store/book[*]/author'
  json: sample
})
console.log('result', result)


//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

// const acorn = require("acorn")
// const ASTQ = require("astq")

// let source = `
//   class Foo {
//     foo () {
//       const bar = "quux"
//       let baz = 42
//     }
//   }
// `

// let ast = acorn.parse(source, { ecmaVersion: 6 })

// let astq = new ASTQ()
// astq.adapter("mozast")
// astq.query(ast, `
//     // VariableDeclarator [
//            /:id   Identifier [ @name  ]
//         && /:init Literal    [ @value ]
//     ]
// `).forEach((node) => {
//   console.log(`${node.id.name}: ${node.init.value}`)
// })

// const acorn = require("acorn")
// const ASTQ = require("astq")
// let source = `
//     class Foo {
//         foo () {
//             const bar = "quux"
//             let baz = 42
//         }
//     }
// `

// let ast = acorn.parse(source, { ecmaVersion: 6 })

// // console.log(JSON.stringify(ast, null, '  '))

// let astq = new ASTQ()
// const hey = astq.query(ast, 'Program')
// // .forEach(function (node) {
// //   console.log(`${node.id.name}: ${node.init.value}`)
// // })

// console.log('hey', hey)