# Babel ES6 Codemod
Note: This is currently written/tested for JSX first ES6 code second. I want this to be extensible/plugable so that it can be done with 

# Features
Given any component [that has JSX] allow an engineer to apply code to said JSX at build time to keep the component clean of any non-business or customer related code. This allows for much easier and faster unit testing (don't have to mock analytics as it isn't required anymore) and cleaner code.

So in the example of analytics one could have all their analytics code outside of thier codebase (say another package) and keep your codebase free of any direct analytics integrations. All configurations for build-time code injection can be saved outside of the codebase as well.

## Configurations
files named `config.bec` (just a jsfile with `.bec` extension)

`.babelrc` configuration file.
```js
{
  "presets": ["babel-preset-expo"],
  "env": {
    "development": {
      "plugins": ["babel-es6-codemod", {
        // JS doesn't have namespacing, unfortunatly, so this is a way
        // to alias paths to make configurations easier and cleaner.
        "namespaces": [{ "some/alias/goes/here": "some/path/goes/here"}],

        // Something about environments because you'll want to run different
        // things on different environments
        "extensions": [".js", ".bec" ], 
        "paths": {
          "config": ["./codemods/analytics", "./codemods/logs"],
          "reporting": "./.reporting",
          "snapshots": "./.snapshots",
        }
      }]
    }
  }
}
```

### For example ...
Given some simple code as such:

```jsx
const Greeting = (name) => (
  <div>
    <UserHeader>
      <Avatar userId={name.user.id} />
      <h1>Hello, {name.given} {name.family}!</h1>
    </UserHeader>
  </div>
)
```

Rewrite it as such:
```jsx
// Because inline is true but if not it'll try to create an import statement.
const genericAnalyticsFunction = (e) => {
  alert(e.target.value)
}

const Greeting = (name) => (
  <h1 onClick={genericAnalyticsFunction}>Hello, {name.given} {name.family}!</h1>
)
```

Using a configuration file similar to:
```js
export default {
  // This will be a filename and path if required. 
  'component.filename.js': {
    // Should understand components
    xpath: '//div/UserHeader/h1'

    // Add it as an attribute, need a smarter way to config this.
    attribute: 'onClick',

    // This may very well be string to start out with honestly.
    code: functionHandler?

    // This is a way to determine if the code is to be pulled into
    // the codemodded file or imported instead.
    inline: true,
  }
}
```

# Features - Future
## Easy on/off
How to turn this on or off globally should be easy to do to make development time easy and fast.

## Snapshots
Save code to know when things were changed.

## Reporting
Which haven't been used yet

## JSON/XPath-ish
Use own custom version of JSONPath so that it uses `//` instead of `..` because `/` is more of a path delimiter.

## Eslint Plugin
Cuz it would be nice to be able to create these and check real-time if the xpath works or not.

## Speed
Ya know it

## VS Code plugin
* Allow the right click of a line of code (or JSX component) and get the XPATH to there.
* A dialogue box that allows for some configuration.

---
# TODO
* Use Lerna to make this a mono repo
* Code & package cleanup upon integration of Lerna
* Link cleanup

---

# Links
* https://jamie.build/babel-plugin-ordering.html
* https://medium.com/@marianococirio/build-your-own-babel-plugin-from-scratch-8db0162f983a
* https://github.com/MFCo/babel-plugin-css-generator-react-components
* https://blog.kentcdodds.com/how-writing-custom-babel-and-eslint-plugins-can-increase-your-productivity-and-improve-user-fd6dd8076e26
* https://hackernoon.com/babel-your-first-code-transformations-2d1a9a2f3bc4
* https://medium.com/the-guild/this-is-how-i-build-babel-plug-ins-b0a13dcd0352
* https://stackoverflow.com/questions/43641032/babel-plugin-how-to-get-the-path-for-a-given-node
* https://tech.mybuilder.com/building-a-babel-plugin-adding-a-function-composition-operator-and-auto-curried-functions-to-javascript/
* https://goessner.net/articles/JsonPath/
* https://www.libhive.com/providers/npm/packages/babel-traverse
* https://halistechnology.com/2015/08/20/a-query-language-for-javascript-objects/

## Packages
* https://github.com/dchester/jsonpath
* https://github.com/s3u/JSONPath (jsonpath-plus)
* https://github.com/goto100/xpath
* https://github.com/Comcast/xml-selector
* https://github.com/rse/astq
* https://github.com/estools/esquery
* https://github.com/sboudrias/AST-query
* https://github.com/deitch/searchjs
* https://github.com/mmckegg/json-query
* https://github.com/ecomfe/babel-plugin-react-add-display-name

