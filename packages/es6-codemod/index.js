const babel = require('@babel/core');
const fs = require('fs');
const plugin = require('./plugin-jsx');
// const plugin = require('./plugin-component');
// const plugin = require('./plugin-component');

// const Generator = require('yeoman-generator');
// const format = require('./format');

// modified manifest file
const source = fs.readFileSync('./__mocks__/component.simple.js', 'utf8');
const result = babel.transform(source, {
  presets: ['@babel/preset-env', '@babel/preset-react'],
  plugins: [
    [plugin, { endpoint: '' }],
  ],
})

fs.writeFileSync('./__mocks__/new-code.js', result.code)

// console.log('code', result.code)
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

// this.fs.write(this.destinationPath('./src/manifest.js'), format(result.code));


// module.exports = class extends Generator {
//   prompting() {
//     const prompts = [
//       {
//         type: 'input',
//         name: 'endpointName',
//         message: 'What is the name of the endpoint?',
//       },
//     ];

//     return this.prompt(prompts).then((props) => {
//       this.props = props;
//     });
//   }

//   writing() {
//     // copy api files to the endpoints directory
//     this.fs.copyTpl(
//       this.templatePath('./endpoint'),
//       this.destinationPath(`./src/endpoints/${this.props.endpointName}`),
//       this.props,
//     );

//     // copy model files to the model directory
//     this.fs.copyTpl(
//       this.templatePath('./model'),
//       this.destinationPath(`./src/models/${this.props.endpointName}`),
//       this.props,
//     );

//     // copy test files to the test directory
//     this.fs.copyTpl(
//       this.templatePath('./test'),
//       this.destinationPath(`./test/unit/${this.props.endpointName}`),
//       this.props,
//     );

//     // modified manifest file
//     const source = fs.readFileSync('./src/manifest.js', 'utf8');
//     const result = babel.transform(source, {
//       plugins: [[plugin, { endpoint: `${this.props.endpointName}` }]],
//     });

//     this.fs.write(this.destinationPath('./src/manifest.js'), format(result.code));
//   }

//   install() {
//     this.npmInstall(['joi', 'elv', 'lm-soap-client']);
//   }
// };