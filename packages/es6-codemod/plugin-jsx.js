// ---------------------------------------------------------------------------
// --- JSX PLUGIN CODE -------------------------------------------------------
// ---------------------------------------------------------------------------
const fs = require('fs');
const babel = require('@babel/core')
const { JSONPath } = require('jsonpath-plus')
const jp = require('jsonpath')
// const { parse, stringify } = require('flatted/cjs')
const { stringify } = require('circular-json')

// All lowercase "just in case" at some point the casing changes.
const isReactElement = (test) => ([
  'jsxattribute',
  'jsxclosingelement',
  'jsxclosingfragment',
  'jsxelement',
  'jsxemptyexpression',
  'jsxexpressioncontainer',
  'jsxfragment',
  'jsxidentifier',
  'jsxmemberexpression',
  'jsxnamespacedname',
  'jsxopeningelement',
  'jsxopeningfragment',
  'jsxspreadattribute',
  'jsxspreadchild',
  'jsxtext',
].includes(test.toLowerCase()))

const isTopLevel = (node) => (!isReactElement(node))

// Plugin takes no arguments.
module.exports = ({ types: t }) => ({
  visitor: {
    Program(path, state) {
      // console.log('state', state)
    },

    // This currently only works for an implicit return JSX component.
    JSXElement(path, state) {
      // console.log(path.node)
      // We ARE on the top level JSX Element, horray!.
      if (isTopLevel(path.parent.type)) {
        console.clear()
        const xPath = '$.children[1].children[1].openingElement'
        // const xPath = '$.children[1].children[1]'
        // const xPath = '$'

        // const result = JSONPath({
        //   path: '$.type.children',
        //   path: '$.children[1].children[1].openingElement',
        //   // path: '$.children[1].children[1].children[0].value',
        //   json: path.node,
        // })

        // path.remove()

        // console.log(path.node)

        const result1 = jp.query(path.node, xPath)
        // const result2 = jp.apply(
        //   path.node,
        
        //   xPath,
        //   (value) => {
        //     value.name.name = 'laksjdlkasjd'
        //     console.log('old node')
        //     console.log(value)
        //     console.log('--------------------------------------------------------------------------------')
        //     // value.remove()
        //     return value
        //   }
        // )

        const clickHandler = () => {}

        console.log(state.addImport)
        // console.log('--------------------------------------------------------------------------------')

        result1[0].attributes.push(
          t.jSXAttribute(
            t.jSXIdentifier('onClick'),
            // Try this: 
            // https://github.com/meowtec/babel-plugin-jsx-classnames/blob/master/src/index.js
            // t.JSXExpressionContainer(
            //   t.callExpression(
            //     // state.addImport('classnames', 'default', 'classNames'),
            //     // [path.node.value.expression]
            //     clickHandler
            //   )),
            t.JSXExpressionContainer(t.expressionStatement(clickHandler))
            // t.JSXExpressionContainer(() => {}),
            // t.jsxEmptyExpression(), 
            // t.stringLiteral('() => { console.log("hey dawg")}')
            // t.declareFunction(clickHandler)
          ),
        )

        // console.log(result1[0])

        // be able to add as attribute
        // parent
        // child
        // inline?
      }

      console.log('--------------------------------------------------------------------------------')
    },
  },
})
