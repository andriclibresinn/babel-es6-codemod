// Include plugin + babel code and run the code (this is a way to test).
// Use multiple plugins as required (for instance any react code).
const source = fs.readFileSync('./src/manifest.js', 'utf8');
const result = babel.transform(source, {
  plugins: [[plugin, { endpoint: `${this.props.endpointName}` }]],
});

this.fs.write(this.destinationPath('./src/manifest.js'), format(result.code));


// ---------------------------------------------------------------------------
// --- PLUGIN CODE -----------------------------------------------------------
// ---------------------------------------------------------------------------
const babel = require('@babel/core');

module.exports = ({ types }, options = { endpoint: 'agreement' }) => {
  // define import statement
  const myImport = babel.template(
    `const ${options.endpoint}= require('./endpoints/${options.endpoint}');`,
    {
      sourceType: 'module',
    },
  );
  
  return {
    visitor: {
      Program(path) {
        // insert import statement
        const lastImport = path
          .get('body')
          .filter((p) => p.isVariableDeclaration())
          .pop();

        if (lastImport) {
          lastImport.insertAfter(myImport());
        }
      },
      ArrowFunctionExpression(path) {
        // insert dependency injection statement
        path
          .get('body')
          .unshiftContainer(
            'body',
            types.variableDeclaration('const', [
              types.VariableDeclarator(
                types.identifier(`${options.endpoint}Routes`),
                types.callExpression(types.identifier(`${options.endpoint}`), [
                  types.identifier('dependencies'),
                ]),
              ),
            ]),
          );
      },
      ArrayExpression(path) {
        // register plugin with the server
        const plugin = types.ObjectExpression([
          types.objectProperty(
            types.identifier('plugin'),
            types.identifier(`${options.endpoint}Routes`),
          ),
        ]);
        path.node.elements.push(plugin);
      },
    },
  };
};